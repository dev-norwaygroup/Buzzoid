import React from "react"
import Image from "next/image"

import Layout from "../layout/layout"
import Seo from "../components/seo"
import { fetchAPI } from "../lib/api"

import Animate from "../components/animate/animate"
import BoxHolder from "../components/box-holder/box-holder"
import FlexRow from "../components/flex-row/flex-row"
import Intro from "../components/intro/intro"
import Logos from "../components/logos/logos"
import Reviews from "../components/reviews/reviews"
import TextSection from "../components/text-section/text-section"
import GridText from "../components/grid-text/grid-text"

const Home = ({ global, homepage }) => {
  const logosArr = [
    {
      src: "/logo1.jpg",
      width: 175,
      height: 38,
    },
    {
      src: "/logo2.jpg",
      width: 186,
      height: 38,
    },
    {
      src: "/logo3.jpg",
      width: 128,
      height: 38,
    },
    {
      src: "/logo4.jpg",
      width: 98,
      height: 38,
    },
    {
      src: "/logo5.jpg",
      width: 100,
      height: 38,
    },
  ]

  const reviewsArr = [
    {
      text: "“Simply love your services, helped me in growing my instagram and every time I had some question everyone answered fast.",
      avatar: "/avatar.png",
      name: "Will Tonenburg",
      position: "Buzzoid.com customer",
    },
    {
      text: "“Simply love your services, helped me in growing my instagram and every time I had some question everyone answered fast.",
      avatar: "/avatar.png",
      name: "Will Tonenburg",
      position: "Buzzoid.com customer",
    },
    {
      text: "“Simply love your services, helped me in growing my instagram and every time I had some question everyone answered fast.",
      avatar: "/avatar.png",
      name: "Will Tonenburg",
      position: "Buzzoid.com customer",
    },
    {
      text: "“Simply love your services, helped me in growing my instagram and every time I had some question everyone answered fast.",
      avatar: "/avatar.png",
      name: "Will Tonenburg",
      position: "Buzzoid.com customer",
    },
    {
      text: "“Simply love your services, helped me in growing my instagram and every time I had some question everyone answered fast.",
      avatar: "/avatar.png",
      name: "Will Tonenburg",
      position: "Buzzoid.com customer",
    },
    {
      text: "“Simply love your services, helped me in growing my instagram and every time I had some question everyone answered fast.",
      avatar: "/avatar.png",
      name: "Will Tonenburg",
      position: "Buzzoid.com customer",
    },
    {
      text: "“Simply love your services, helped me in growing my instagram and every time I had some question everyone answered fast.",
      avatar: "/avatar.png",
      name: "Will Tonenburg",
      position: "Buzzoid.com customer",
    },
  ]

  return (
    <Layout global={global} className="home">
      <Seo seo={homepage.seo} />
      <Animate>
        <Intro first />
      </Animate>
      <Animate>
        <Logos
          logos={logosArr}
          title="Trusted by Publishing Influencer All Over The Net"
        />
      </Animate>
      <Animate>
        <TextSection className="is-center">
          <h2>Why is Buzzoid Different?</h2>
          <p>
            We serve thousands of customers on a daily basis and
            <br /> we&apos;re proud to have high customer loyalty.
          </p>
        </TextSection>
      </Animate>
      <BoxHolder>
        <Animate>
          <GridText>
            <GridText.Item>
              <h5>Why Buzzoid</h5>
              <h4>Buzzoid is a Trusted Marketing Pricing</h4>
            </GridText.Item>
            <GridText.Item rows={2} order={2} className="is-center">
              <Image
                src="/img1.png"
                width={419}
                height={397}
                alt="Why Buzzoid"
              />
            </GridText.Item>
            <GridText.Item className="mb-full">
              <p>
                We serve thousands of customers on a daily basis and we&apos;re proud
                to have high customer loyalty. Our competitive pricing and
                around-the-clock support are a few out of many reasons for why
                our customers keep coming back.
              </p>
            </GridText.Item>
          </GridText>
        </Animate>
        <Animate>
          <GridText>
            <GridText.Item>
              <h5>How is it Different</h5>
              <h4>Buzzoid is a Trusted Marketing Servcies</h4>
            </GridText.Item>
            <GridText.Item rows={2} className="is-center">
              <Image
                src="/img2.png"
                width={445}
                height={369}
                alt="Why Buzzoid"
              />
            </GridText.Item>
            <GridText.Item className="mb-full">
              <p>
                We serve thousands of customers on a daily basis and we&apos;re proud
                to have high customer loyalty. Our competitive pricing and
                around-the-clock support are a few out of many reasons for why
                our customers keep coming back.
              </p>
            </GridText.Item>
          </GridText>
        </Animate>
        <Animate>
          <GridText className="is-last">
            <GridText.Item>
              <h5>Getting Started</h5>
              <h4>Buzzoid is a Trusted Marketing Guarantee</h4>
            </GridText.Item>
            <GridText.Item rows={2} order={2} className="is-center">
              <Image
                src="/img3.png"
                width={430}
                height={352}
                alt="Why Buzzoid"
              />
            </GridText.Item>
            <GridText.Item className="mb-full">
              <p>
                We serve thousands of customers on a daily basis and we&apos;re proud
                to have high customer loyalty. Our competitive pricing and
                around-the-clock support are a few out of many reasons for why
                our customers keep coming back.
              </p>
            </GridText.Item>
          </GridText>
        </Animate>
      </BoxHolder>
      <Animate>
        <Reviews title="Don’t just take our word for it" reviews={reviewsArr} />
      </Animate>
    </Layout>
  )
}

export async function getStaticProps() {
  // Run API calls in parallel
  const [global, homepage] = await Promise.all([
    fetchAPI("/global"),
    fetchAPI("/homepage"),
  ])

  return {
    props: { homepage, global },
    revalidate: 1,
  }
}

export default Home
