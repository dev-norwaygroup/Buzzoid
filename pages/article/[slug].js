import ReactMarkdown from "react-markdown"
import Moment from "react-moment"
import { fetchAPI } from "../../lib/api"
import Layout from "../../layout/layout"
import NextImage from "../../components/image"
import Seo from "../../components/seo"
import { getStrapiMedia } from "../../lib/media"
import { uriTransformer } from "../../lib/uri-transform"
import FlexRow from "../../components/flex-row/flex-row"
import Comparison from "../../components/comparison/comparison"

const Article = ({ article, global }) => {
  const imageUrl = getStrapiMedia(article.image)

  const seo = {
    metaTitle: article.title,
    metaDescription: article.description,
    shareImage: article.image,
    article: true,
  }

  const imageObj = {
    ...article.author?.picture,
    width: 51,
    height: 51,
  }

  return (
    <Layout global={global} className="article">
      <Seo seo={seo} />

      <div className="article">
        <div className="shell">
          <h5 className="article__top-text is-center">{article.top_text}</h5>
          <div className="article__head">
            <h1 className="article__title">{article.title}</h1>
            {article.description && <p>{article.description}</p>}
          </div>
          <FlexRow center align_center className="article__author" gap={13}>
            <FlexRow.Col>
              {article.author?.picture && (
                <NextImage image={imageObj} fit="cover" />
              )}
            </FlexRow.Col>
            <FlexRow.Col>
              <p>
                By {article.author?.name}
                <br />{" "}
                <Moment format="MMM Do YYYY">{article.published_at}</Moment>
              </p>
            </FlexRow.Col>
          </FlexRow>
          <div className="article__image">
            <NextImage
              image={{
                ...article.image,
                url: imageUrl,
              }}
            />
          </div>
          <div className="article__content">
            <ReactMarkdown
              source={article.content}
              escapeHtml={false}
              transformImageUri={uriTransformer}
            />
          </div>
          <Comparison data={article} />
        </div>
      </div>
    </Layout>
  )
}

export async function getStaticPaths() {
  const articles = await fetchAPI("/articles")

  return {
    paths: articles.map((article) => ({
      params: {
        slug: article.slug,
      },
    })),
    fallback: false,
  }
}

export async function getStaticProps({ params }) {
  const global = await fetchAPI("/global")
  const articles = await fetchAPI(`/articles?slug=${params.slug}`)

  return {
    props: { article: articles[0], global },
    revalidate: 1,
  }
}

export default Article
