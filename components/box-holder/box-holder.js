import classNames from "classnames"

const BoxHolder = ({ color, children, className }) => {
  return (
    <div className={classNames("box-holder", color, className)}>
      <div className="shell">
        <div className="box-holder__inner">{children}</div>
      </div>
    </div>
  )
}

export default BoxHolder
