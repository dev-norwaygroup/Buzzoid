import Link from "next/link"
import classNames from "classnames"

const Button = ({ children, href, className, small, color, large }) => (
  <Link href={href}>
    <a
      className={classNames("btn", className, {
        "is-small": small,
        "is-large": large,
        [`btn--color-${color}`]: color,
      })}
    >
      <span className="btn__inner">{children}</span>
    </a>
  </Link>
)

export default Button
