const SkeletonText = ({ width }) => (
  <div className="skeleton-text" style={{ width: width }} />
)

export default SkeletonText
