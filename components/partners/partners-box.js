import Image from 'next/image'
import Link from 'next/link'

const PartnersBox = ({ image, title, url }) => (
  <div className="partners-box">
    <div className="partners-box__inner">
      <div className="partners-box__image">
        <Image src={image} width={214} height={142} />
      </div>
      <div className="partners-box__content">
        <div className="partners-box__title">{title}</div>
        <Link href={url}>
          <a className="partners-box__more">Read More</a>
        </Link>
      </div>
    </div>
  </div>
)

export default PartnersBox
