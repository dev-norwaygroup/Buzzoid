import React from 'react'
import PartnersBox from './partners-box'

const Partners = ({ title, partners }) => {
  return (
    <div className="partners">
      <div className="shell">
        {title && <h4 className="is-center">{title}</h4>}
        <div className="partners__inner">
          {partners.map(({image, title, url}, index) => <PartnersBox key={index} image={image} title={title} url={url} />)}
        </div>
      </div>
    </div>
  )
}

export default Partners
