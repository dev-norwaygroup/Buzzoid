import React from 'react'

const ComparisonIcon = ({ flag }) => {
  return (
    <div className="comparison-icon">
      {flag 
      ? <svg width="23" height="22" viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fillRule="evenodd" clipRule="evenodd" d="M3.29591 10.3526C4.8575 11.6159 6.4191 12.8127 7.98069 14.0095C11.2991 9.4218 14.6825 4.9006 18.0009 0.379395C19.3673 1.37672 20.6686 2.37404 22.035 3.43786C17.6756 9.28883 13.3161 15.2063 8.95669 21.1237C6.09376 18.8631 3.16578 16.669 0.302856 14.4084C1.27885 13.0787 2.25485 11.7489 3.29591 10.3526Z" fill="#FF6B00"/>
        </svg>
      : <svg width="26" height="3" viewBox="0 0 26 3" fill="none" xmlns="http://www.w3.org/2000/svg">
          <line opacity="0.1" x1="0.000732422" y1="1.5" x2="26.0007" y2="1.5" stroke="black" strokeWidth="3"/>
        </svg>
      }
    </div>
  )
}

export default ComparisonIcon
