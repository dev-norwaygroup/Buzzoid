import ComparisonIcon from "./comparison-icon"

const Comparison = ({ data }) => {
  const {
    compare_columns: { column1, column2, compare_subtitle, compare_title },
    compare_rows,
  } = data
  return (
    <div className="comparison">
      <div className="comparison__head">
        {compare_title && (
          <div className="comparison__title">{compare_title}</div>
        )}
        {compare_subtitle && <p>{compare_subtitle}</p>}
      </div>
      <div className="comparison__table">
        <div className="comparison__table-col">
          <div className="comparison__table-head">Feature</div>
          {compare_rows.map((item) => (
            <div className="comparison__table-item" key={item.feature}>
              {item.feature}
            </div>
          ))}
        </div>
        <div className="comparison__table-col">
          <div className="comparison__table-head">
            <strong>{column1}</strong>
          </div>
          {compare_rows.map((item) => (
            <div className="comparison__table-item" key={item.feature}>
              <ComparisonIcon flag={item.column1} />
            </div>
          ))}
        </div>
        <div className="comparison__table-col">
          <div className="comparison__table-head">
            <strong>{column2}</strong>
          </div>
          {compare_rows.map((item) => (
            <div className="comparison__table-item" key={item.feature}>
              <ComparisonIcon flag={item.column2} />
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default Comparison
