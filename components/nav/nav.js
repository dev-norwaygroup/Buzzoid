import classNames from "classnames"
import Link from "next/link"

const Nav = ({ links }) => {
  const splitItems = (text) =>
    text.split("").map((item, index) => (
      <span
        style={{ animationDelay: `${(index + 1) * 20}ms` }}
        className={classNames("nav__char", { "is-empty": item === " " })}
        key={index}
      >
        {item}
      </span>
    ))
  return (
    <nav className="nav">
      <ul>
        {links?.map((item) => (
          <li key={item.id}>
            <Link href={item.URL}>
              <a>{splitItems(item.Title)}</a>
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  )
}

export default Nav
