import classNames from 'classnames'

import GridTextItem from './grid-text-item'

const GridText = ({children, className}) => (
  <div className={classNames('grid-text', className)}>
    {children}
  </div>
)

GridText.Item = GridTextItem;

export default GridText
