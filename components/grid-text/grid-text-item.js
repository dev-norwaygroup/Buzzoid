import classNames from "classnames"

const GridTextItem = ({ children, className, rows, order }) => (
  <div
    className={classNames("grid-text__item", className, {
      [`grid-text__item--rows-${rows}`]: rows,
      [`grid-text__item--order-${order}`]: order,
    })}
  >
    {children}
  </div>
)

export default GridTextItem
