import Image from 'next/image'

const ReviewsStars = ({ count }) => {
  const arr = Array.from(Array(count).keys());
  return (
    <div className="reviews__stars">
      {arr.map((item, index) => <span key={index}><Image src="/icon-star.svg" width={16} height={16} alt="review star" /></span>)}
    </div>
  )
}

export default ReviewsStars
