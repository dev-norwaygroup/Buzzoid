import FlexRow from "../flex-row/flex-row"
import Image from "next/image"

const ReviewsBox = ({ children, avatar, name, position }) => {
  const avatarSrc = avatar || "/avatar.png"

  return (
    <div className="reviews-box">
      <div className="reviews-box__inner">
        <div className="reviews-box__text is-center">{children}</div>
        <div className="reviews-box__author">
          <FlexRow gap={16} align_center>
            <FlexRow.Col>
              <Image src={avatarSrc} width={39} height={39} alt={name} />
            </FlexRow.Col>
            <FlexRow.Col>
              {name && <h6>{name}</h6>}
              {position && <small>{position}</small>}
            </FlexRow.Col>
          </FlexRow>
        </div>
      </div>
    </div>
  )
}

export default ReviewsBox
