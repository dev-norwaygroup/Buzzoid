import { useState, useCallback, useEffect } from "react"
import useEmblaCarousel from "embla-carousel-react"
import FlexRow from "../flex-row/flex-row"
import ReviewsStars from "./reviews-stars"
import ReviewsBox from "./reviews-box"

const Reviews = ({ reviews, title }) => {
  const [viewportRef, embla] = useEmblaCarousel({
    align: 0,
    skipSnaps: false,
    slidesToScroll: 1,
    containScroll: "trimSnaps",
  })

  const scrollPrev = () => embla.scrollPrev()
  const scrollNext = () => embla.scrollNext()

  return (
    <div className="reviews">
      <div className="shell">
        <div className="reviews__inner">
          {title && <h4>{title}</h4>}
          <div className="reviews__head">
            <FlexRow center gap="15px" align_center mobile>
              <FlexRow.Col>
                <ReviewsStars count={5} />
              </FlexRow.Col>
              <FlexRow.Col>
                <p>Rated 5.0 for Customer Service</p>
              </FlexRow.Col>
            </FlexRow>
          </div>
        </div>
      </div>
      <div className="embla" ref={viewportRef}>
        <div className="embla__container">
          {reviews.map((item, index) => (
            <ReviewsBox
              key={index}
              avatar={item.avatar}
              name={item.name}
              position={item.position}
            >
              <p>{item.text}</p>
            </ReviewsBox>
          ))}
        </div>
      </div>
      <div className="embla__cta">
        <button onClick={scrollPrev} className="prev"></button>
        <button onClick={scrollNext} className="next"></button>
      </div>
    </div>
  )
}

export default Reviews
