import Image from 'next/image'

import FlexRow from '../flex-row/flex-row'
import SkeletonText from '../skeleton-text/skeleton-text'

const Card = ({ image, alt, children }) => {
  return (
    <div className="card">
      <div className="card__inner">
        <div className="card__image">
          <Image src={image} alt={alt} layout="fill" />
        </div>
      </div>
      <div className="card__content">
        <FlexRow gap="15px">
          <FlexRow.Col>
            <div className="card__heart">
              <Image src="/icon-heart.svg" width={28} height={23} alt="heart" />
            </div>
          </FlexRow.Col>
          <FlexRow.Col grow>
            <SkeletonText width="69%" />
            <SkeletonText width="38%" />
          </FlexRow.Col>
        </FlexRow>
      </div>
      
    </div>
  )
}

export default Card
