import { useEffect, useState } from "react"

import Card from "../card/card"
import PhoneFrame from "./phone-frame"
import PhoneBg from "./phone-bg"
import classNames from "classnames"

const Phone = ({ className }) => {
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    setLoaded(true)
  }, [])

  return (
    <div className={classNames("phone", className, { "is-loaded": loaded })}>
      <PhoneBg />
      <PhoneFrame>
        <Card image="/card-image1.jpg" />
      </PhoneFrame>
    </div>
  )
}

export default Phone
