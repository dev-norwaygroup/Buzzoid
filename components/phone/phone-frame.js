const PhoneFrame = ({ children }) => (
  <div className="phone__frame">
    <svg
      width="372"
      height="467"
      viewBox="0 0 372 467"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g filter="url(#filter0_d_11:210)">
        <rect
          x="16.6997"
          y="5"
          width="314.601"
          height="643"
          rx="47.834"
          fill="#C4C4C4"
        />
        <rect
          x="14.5207"
          y="2.82103"
          width="318.959"
          height="647.358"
          rx="50.013"
          stroke="#FF5C00"
          strokeWidth="4.35793"
        />
      </g>
      <rect
        x="16.6997"
        y="5"
        width="314.601"
        height="643"
        rx="47.834"
        fill="#151515"
      />
      <rect
        x="20.3792"
        y="9.59937"
        width="308.162"
        height="631.961"
        rx="47.834"
        fill="white"
      />
      <mask
        id="mask0_11:210"
        maskUnits="userSpaceOnUse"
        x="1"
        y="9"
        width="346"
        height="633"
      >
        <rect
          x="1.98145"
          y="9.59937"
          width="344.037"
          height="631.961"
          rx="47.834"
          fill="white"
        />
      </mask>
      <g mask="url(#mask0_11:210)">
        <rect
          x="102.142"
          y="-18.7991"
          width="144.901"
          height="56.6532"
          rx="28.3266"
          fill="#151515"
        />
      </g>
      <defs>
        <filter
          id="filter0_d_11:210"
          x="0.790998"
          y="0.64209"
          width="370.675"
          height="699.652"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dx="12.1283" dy="24.2567" />
          <feGaussianBlur stdDeviation="11.8396" />
          <feComposite in2="hardAlpha" operator="out" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0.489635 0 0 0 0 0.491743 0 0 0 0 0.495833 0 0 0 0.37 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow_11:210"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow_11:210"
            result="shape"
          />
        </filter>
      </defs>
    </svg>
    {children}
  </div>
)

export default PhoneFrame
