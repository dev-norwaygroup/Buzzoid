import classNames from "classnames"

import Button from "../button/button"
import FlexRow from "../flex-row/flex-row"
import Phone from "../phone/phone"

const Intro = ({ first }) => (
  <div className={classNames("intro", { "is-first": first })}>
    <div className="shell">
      <div className="intro__inner">
        <FlexRow between align_center mobile>
          <FlexRow.Col grid={6} className="intro__content">
            <h1>Looking for Buzzoid?</h1>
            <p>
              Social media is exploding and a revolution is going on that’s
              changing the way consumers interact.
            </p>
            <Button href="#" color="orange">
              Visit Buzzoid.com
            </Button>
          </FlexRow.Col>
          <FlexRow.Col grid={6} className="is-center">
            <Phone />
          </FlexRow.Col>
        </FlexRow>
      </div>
    </div>
  </div>
)

export default Intro
