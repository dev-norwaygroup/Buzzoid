const Animate = ({ children }) => <div  data-aos="fade-up">{children}</div>

export default Animate
