import Image from "next/image"

const Logos = ({ logos, title }) => {
  return (
    <div className="logos">
      <div className="shell">
        {title && <small>{title}</small>}
        <div className="logos__inner">
          {logos.map((item, index) => (
            <div className="logos_image" key={index}>
              <Image src={item} alt="logo" />
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default Logos
