import classNames from "classnames"

const TextSection = ({ children, className }) => {
  return (
    <div className={classNames("text-section", className)} data-aos="fade-up">
      <div className="shell">
        {children}
      </div>
    </div>
  )
}

export default TextSection
