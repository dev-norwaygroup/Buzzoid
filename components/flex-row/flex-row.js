import classNames from "classnames"
import React from "react"

const FlexRowCol = ({ children, className, grid, grow, order }) => (
  <div
    className={classNames("flex-col", className, {
      [`flex-col--grid-${grid}`]: grid,
      ["flex-col--grow"]: grow,
      ["flex-col--order"]: order,
    })}
  >
    {children}
  </div>
)

const FlexRow = ({
  children,
  className,
  between,
  align_center,
  gap,
  center,
  mobile,
}) => {
  return (
    <div
      className={classNames("flex-row", className, {
        "flex-row--between": between,
        "flex-row--align-center": align_center,
        "flex-row--center": center,
        "flex-row--mobile-block": mobile,
      })}
      style={{ gap: gap }}
    >
      {children}
    </div>
  )
}

FlexRow.Col = FlexRowCol

export default FlexRow
