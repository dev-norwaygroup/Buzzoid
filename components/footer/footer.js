import ReactMarkdown from "react-markdown"
import Button from "../button/button"

const Footer = ({
  data: { button_text, button_url, copy, description, small_title, title },
}) => (
  <footer className="footer is-center">
    <div className="shell">
      <div className="footer__top">
        {small_title && <h5>{small_title}</h5>}
        {title && <h3>{title}</h3>}
        <Button href={button_url} color="black">
          {button_text}
        </Button>
      </div>
      <div className="footer__bottom">
        <ReactMarkdown source={description} escapeHtml={false} />
        <ReactMarkdown source={copy} escapeHtml={false} />
      </div>
    </div>
  </footer>
)

export default Footer
