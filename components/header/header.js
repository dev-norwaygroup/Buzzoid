import Link from "next/link"

import Logo from "../logo/logo"
import Nav from "../nav/nav"
import Button from "../button/button"

const Header = ({ links }) => (
  <div className="header">
    <div className="shell">
      <div className="header__inner">
        <Logo />
        <Nav links={links} />
        <div className="header__cta">
          <Button href="/" small color="white">
            Get Free Trial
          </Button>
        </div>
      </div>
    </div>
  </div>
)

export default Header
