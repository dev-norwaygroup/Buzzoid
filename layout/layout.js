import { useRouter } from "next/router"
import classNames from "classnames"

import Header from "../components/header/header"
import Footer from "../components/footer/footer"
import Animate from "../components/animate/animate"
import BoxHolder from "../components/box-holder/box-holder"
import Partners from "../components/partners/partners"
import TextSection from "../components/text-section/text-section"
import Phone from "../components/phone/phone"
import Button from "../components/button/button"

const Layout = ({ children, global, className }) => {
  const router = useRouter()

  const partnersArr = [
    {
      image: "/part1.jpg",
      title: "Buzzoid vs Stormlikes: Comparison and Takeaways",
      url: "/",
    },
    {
      image: "/part2.jpg",
      title: "Buzzoid vs Stormlikes: Comparison and Takeaways",
      url: "/",
    },
    {
      image: "/part3.jpg",
      title: "Buzzoid vs Stormlikes: Comparison and Takeaways",
      url: "/",
    },
    {
      image: "/part4.jpg",
      title: "Buzzoid vs Stormlikes: Comparison and Takeaways",
      url: "/",
    },
  ]

  return (
    <>
      <Header links={global.navigation} />
      <main
        className={classNames("main-content", `main-content--${className}`)}
      >
        {children}
      </main>
      <Animate>
        <BoxHolder className="mb-100 box-holder--tighter">
          <Partners
            title="Comparisons from out Partners"
            partners={partnersArr}
          />
        </BoxHolder>
      </Animate>
      <Animate>
        <TextSection className="is-center">
          <h2>Get Started with a Free Trial!</h2>
          <p>
            We serve thousands of customers on a daily basis and we're proud to
            go.
          </p>
          <Button href="#" color="white" large>
            Get a Free Trial
          </Button>
        </TextSection>
      </Animate>
      <Animate>
        <Phone className="phone--center phone--scale" />
      </Animate>
      <Footer data={global.Footer} />
    </>
  )
}

export default Layout
